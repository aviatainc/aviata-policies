# Aviata Policies
This repo is a record of any company policies.

## Privacy Policy found at www.aviatainc.com/privacy-policy/.

The privacy policy isn't captured in the site repo (https://bitbucket.org/aviatainc/aviata-website/src/master/) instead it's put in with some sort of Wordpress magic. So, it was added here so have a record of it.

It can be edited here: http://www.aviatainc.com/wp-admin/post.php?post=1660&action=edit